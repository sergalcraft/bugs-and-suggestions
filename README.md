# **Bug Reporting** and **Suggestions** for ***SergalCraft!***

Okay guys, so this is REALLY EASY, and it makes things less of a clusterfuck to keep track of.

---

## **STEP 1**
Click on this icon in the sidebar

![Step 1](https://dl.dropboxusercontent.com/s/nrpkja1z4g3651n/001.png?dl=0)

or [this link here](https://bitbucket.org/sergalcraft/bugs-and-suggestions/issues)

## **STEP 2**
Click on this in the top-right corner

![Step 2](https://dl.dropboxusercontent.com/s/1k55qx5uqh4s083/002.png?dl=0)

## **STEP 3**
Follow the instructions listed on the page.

---

# *Easy peasy!*